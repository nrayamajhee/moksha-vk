use cgmath::{
    perspective, Decomposed, Deg, InnerSpace, Matrix4, One, Point3, Quaternion, Rad, Rotation,
    Rotation3, Transform, Vector3,
};
use std::ops::Range;
use winit::{ElementState, MouseButton};
use crate::events::ViewportEvent;

#[derive(Debug)]
pub struct PerspectiveProjection {
    pub fov: f32,
    pub range: Range<f32>,
}

impl PerspectiveProjection {
    pub fn new(fov: f32, near: f32, far: f32) -> Self {
        Self {
            fov,
            range: Range {
                start: near,
                end: far,
            },
        }
    }
    pub fn matrix(&self, aspect_ratio: f32) -> Matrix4<f32> {
        perspective(
            Rad::from(Deg(self.fov)),
            aspect_ratio,
            self.range.start,
            self.range.end,
        )
    }
}
pub struct DynamicViewport {
    projection: PerspectiveProjection,
    pub projection_matrix: Matrix4<f32>,
    pub view_matrix: Matrix4<f32>,
    transform: Decomposed<Vector3<f32>, Quaternion<f32>>,
    initial_transform: Decomposed<Vector3<f32>, Quaternion<f32>>,
    delta_rot: Quaternion<f32>,
    delta_scale: f32,
    pub target: [f32; 3],
    pub smooth: bool,
    sensitivity: f32,
    pub button: Option<MouseButton>,
    reset: bool,
}
impl DynamicViewport {
    pub fn new(proj: PerspectiveProjection, dimensions: [u32; 2]) -> Self {
        let position = [0.0, 0.0, -3.0];
        let target = [0.0, 0.0, 0.0];
        let up = Vector3::unit_y();

        let projection_matrix = proj.matrix(dimensions[0] as f32 / dimensions[1] as f32);
        let rotation = Quaternion::look_at(Point3::from(position) - Point3::from(target), up);
        let transform = Decomposed {
            disp: position.into(),
            rot: rotation,
            ..Decomposed::one()
        };

        let view = Matrix4::look_at(position.into(), target.into(), up);

        Self {
            projection: proj,
            projection_matrix,
            view_matrix: view,
            smooth: true,
            transform,
            initial_transform: transform,
            target: target,
            delta_rot: Quaternion::one(),
            delta_scale: 0.,
            sensitivity: 1.0,
            button: Some(MouseButton::Left),
            reset: false,
        }
    }
    pub fn update(&mut self, event: &ViewportEvent) {
        match event.aspect_ratio {
            Some(r) => self.update_projection(r as f32),
            None => (),
        }
        match event.increment_sensitivity {
            Some((reset, val)) => {
                if reset {
                    self.restore_sensitivity();
                } else {
                    self.increment_sensitivity(val);
                }
            }
            None => (),
        }
        let (dx, dy) = event.mouse_event.cursor_delta;
        let ds = event.mouse_event.scroll_delta;
        let allowed = match self.button {
            Some(button) => {
                if button == event.mouse_event.button.unwrap()  && event.mouse_event.state == ElementState::Pressed {
                    true
                } else {
                    false
                }
            } None => {
                true
            }
        };
        self.update_view(dx, dy, ds, allowed);
        if event.reset_view {
            self.reset_view();
        }
    }
    pub fn update_view(&mut self, dx: f64, dy: f64, ds: f32, allowed: bool) {
        if !self.reset {
            let get_delta_rot = || {
                let axis = self.transform.rot * Vector3::unit_y();
                let q_hor = Quaternion::from_axis_angle(
                    axis,
                    Deg(self.sensitivity * 0.2 * -dx as f32),
                );
                let axis = self.transform.rot * Vector3::unit_x();
                let q_ver = Quaternion::from_axis_angle(
                    axis,
                    Deg(self.sensitivity * 0.2 * -dy as f32),
                );
                q_hor * q_ver
            };
            let delta = if allowed {get_delta_rot()} else {Quaternion::one()};
            let scale_d = 0.05 * self.sensitivity;
            if self.smooth {
                self.delta_rot = self.delta_rot.nlerp(delta, 0.05);
                if self.delta_scale == 0. {
                    self.delta_scale = ds * scale_d / 10.;
                } else {
                    let sign = self.delta_scale / f32::abs(self.delta_scale);
                    self.delta_scale = if f32::abs(self.delta_scale) > scale_d {
                        0.
                    } else {
                        self.delta_scale + sign * scale_d / 10.
                    }
                }
            } else {
                self.delta_rot = delta;
                self.delta_scale = ds * scale_d;
            }
        } else {
            let dist_t_i =
                f32::abs(self.transform.disp.magnitude() - self.initial_transform.disp.magnitude());
            let align_t_i =
                f32::abs(1. - f32::powi(self.transform.rot.dot(self.initial_transform.rot), 2));
            // println!("{} {}", dist_t_i, align_t_i);
            if dist_t_i < 0.1 || dist_t_i.is_nan() {
                self.delta_scale = 0.;
                if align_t_i < 0.01 || align_t_i.is_nan() {
                    self.delta_rot = Quaternion::one();
                    self.transform = self.initial_transform;
                    self.reset = false;
                }
            }
        }
        self.transform.disp = self.transform.disp - Vector3::from(self.target);
        self.transform.rot = self.delta_rot * self.transform.rot;
        if self.delta_scale == 0. {
            self.transform.disp =
                self.delta_rot.rotate_vector(self.transform.disp) + Vector3::from(self.target);
        } else {
            let next_disp = [0.0, 0.0, self.transform.disp.magnitude() + self.delta_scale].into();
            self.transform.disp =
                self.transform.rot.rotate_vector(next_disp) + Vector3::from(self.target);
        };
        let position: [f32; 3] = self.transform.disp.into();
        self.view_matrix = Matrix4::look_at(
            position.into(),
            self.target.into(),
            self.transform.rot * Vector3::unit_y(),
        );
    }
    pub fn update_projection(&mut self, aspect_ratio: f32) {
        self.projection_matrix = self.projection.matrix(aspect_ratio);
    }
    pub fn reset_view(&mut self) {
        self.delta_rot = Quaternion::one().nlerp(
            self.initial_transform.rot * self.transform.rot.invert(),
            0.1,
        );
        let diff = self.initial_transform.disp.magnitude() - self.transform.disp.magnitude();
        self.delta_scale = (diff / f32::abs(diff)) * 0.1;
        self.reset = true;
    }
    pub fn increment_sensitivity(&mut self, d: Option<f32>) {
        let after = self.sensitivity + d.unwrap_or(1.);
        if  after > 0. {
            self.sensitivity = after;
        }
    }
    pub fn restore_sensitivity(&mut self) {
        self.sensitivity = 1.;
    }
}
