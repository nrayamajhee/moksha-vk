use crate::{
    geometry::update_uniform_buffer, viewport::DynamicViewport, QueueFamilyIndices,
    UniformBufferObject, Vertex,
};
use std::collections::HashSet;
use std::sync::{Arc, Mutex};
use std::time::Instant;
use vulkano::{
    buffer::{
        immutable::ImmutableBuffer, BufferAccess, BufferUsage, CpuAccessibleBuffer,
        TypedBufferAccess,
    },
    command_buffer::{AutoCommandBuffer, AutoCommandBufferBuilder, DynamicState},
    descriptor::descriptor_set::{
        FixedSizeDescriptorSet, FixedSizeDescriptorSetsPool, PersistentDescriptorSetBuf,
        PersistentDescriptorSetImg, PersistentDescriptorSetSampler,
    },
    device::{Device, DeviceExtensions, Features, Queue},
    format::{ClearValue, Format},
    framebuffer::{Framebuffer, FramebufferAbstract, RenderPassAbstract, Subpass},
    image::{swapchain::SwapchainImage, AttachmentImage, ImageAccess, ImageUsage, ImmutableImage},
    instance::{
        debug::{DebugCallback, MessageTypes},
        layers_list, ApplicationInfo, Instance, InstanceExtensions, PhysicalDevice, Version,
    },
    pipeline::{
        depth_stencil::DepthStencil, viewport::Viewport, GraphicsPipeline, GraphicsPipelineAbstract,
    },
    sampler::Sampler,
    single_pass_renderpass,
    swapchain::{
        Capabilities, ColorSpace, CompositeAlpha, PresentMode, SupportedPresentModes, Surface,
        Swapchain,
    },
    sync::{self, GpuFuture, SharingMode},
};
use winit::Window;

#[cfg(all(debug_assertions))]
const ENABLE_VALIDATION_LAYERS: bool = true;
#[cfg(not(debug_assertions))]
const ENABLE_VALIDATION_LAYERS: bool = false;

const VALIDATION_LAYERS: &[&str] = &["VK_LAYER_LUNARG_standard_validation"];

type DescriptorSetUBO = PersistentDescriptorSetBuf<Arc<CpuAccessibleBuffer<UniformBufferObject>>>;
type DescriptorSetImage = PersistentDescriptorSetImg<Arc<ImmutableImage<Format>>>;
type DescriptorSetResources = (
    (((), DescriptorSetUBO), DescriptorSetImage),
    PersistentDescriptorSetSampler,
);
pub type DescriptorSet = Vec<
    Arc<
        FixedSizeDescriptorSet<Arc<GraphicsPipelineAbstract + Send + Sync>, DescriptorSetResources>,
    >,
>;

fn device_extensions() -> DeviceExtensions {
    DeviceExtensions {
        khr_swapchain: true,
        ..vulkano::device::DeviceExtensions::none()
    }
}

pub fn create_instance() -> Arc<Instance> {
    if ENABLE_VALIDATION_LAYERS && !check_validation_layer_support() {
        println!("Validation layers requested, but not available!")
    }

    let supported_extensions =
        InstanceExtensions::supported_by_core().expect("failed to retrieve supported extensions");
    println!("Supported extensions: {:?}", supported_extensions);

    let app_info = ApplicationInfo {
        application_name: Some("Hello Triangle".into()),
        application_version: Some(Version {
            major: 1,
            minor: 0,
            patch: 0,
        }),
        engine_name: Some("No Engine".into()),
        engine_version: Some(Version {
            major: 1,
            minor: 0,
            patch: 0,
        }),
    };

    let required_extensions = get_required_extensions();

    if ENABLE_VALIDATION_LAYERS && check_validation_layer_support() {
        Instance::new(
            Some(&app_info),
            &required_extensions,
            VALIDATION_LAYERS.iter().map(|s| *s),
        )
        .expect("failed to create Vulkan instance")
    } else {
        Instance::new(Some(&app_info), &required_extensions, None)
            .expect("failed to create Vulkan instance")
    }
}

fn check_validation_layer_support() -> bool {
    let layers: Vec<_> = layers_list()
        .unwrap()
        .map(|l| l.name().to_owned())
        .collect();
    VALIDATION_LAYERS
        .iter()
        .all(|layer_name| layers.contains(&layer_name.to_string()))
}

fn get_required_extensions() -> InstanceExtensions {
    let mut extensions = vulkano_win::required_extensions();
    if ENABLE_VALIDATION_LAYERS {
        // TODO!: this should be ext_debug_utils (_report is deprecated), but that doesn't exist yet in vulkano
        extensions.ext_debug_report = true;
    }

    extensions
}

pub fn setup_debug_callback(instance: &Arc<Instance>) -> Option<DebugCallback> {
    if !ENABLE_VALIDATION_LAYERS {
        return None;
    }

    let msg_types = MessageTypes {
        error: false,
        warning: false,
        performance_warning: false,
        information: false,
        debug: false,
    };
    DebugCallback::new(&instance, msg_types, |msg| {
        println!("validation layer: {:?}", msg.description);
    })
    .ok()
}

pub fn pick_physical_device(instance: &Arc<Instance>, surface: &Arc<Surface<Window>>) -> usize {
    PhysicalDevice::enumerate(&instance)
        .position(|device| is_device_suitable(surface, &device))
        .expect("failed to find a suitable GPU!")
}

fn is_device_suitable(surface: &Arc<Surface<Window>>, device: &PhysicalDevice) -> bool {
    let indices = find_queue_families(surface, device);
    let extensions_supported = check_device_extension_support(device);

    let swap_chain_adequate = if extensions_supported {
        let capabilities = surface
            .capabilities(*device)
            .expect("failed to get surface capabilities");
        !capabilities.supported_formats.is_empty()
            && capabilities.present_modes.iter().next().is_some()
    } else {
        false
    };

    indices.is_complete() && extensions_supported && swap_chain_adequate
}

fn find_queue_families(
    surface: &Arc<Surface<Window>>,
    device: &PhysicalDevice,
) -> QueueFamilyIndices {
    let mut indices = QueueFamilyIndices::new();
    // TODO: replace index with id to simplify?
    for (i, queue_family) in device.queue_families().enumerate() {
        if queue_family.supports_graphics() {
            indices.graphics_family = i as i32;
        }

        if surface.is_supported(queue_family).unwrap() {
            indices.present_family = i as i32;
        }

        if indices.is_complete() {
            break;
        }
    }

    indices
}

fn check_device_extension_support(device: &PhysicalDevice) -> bool {
    let available_extensions = DeviceExtensions::supported_by_device(*device);
    let device_extensions = device_extensions();
    available_extensions.intersection(&device_extensions) == device_extensions
}

pub fn create_logical_device(
    instance: &Arc<Instance>,
    surface: &Arc<Surface<Window>>,
    physical_device_index: usize,
) -> (Arc<Device>, Arc<Queue>, Arc<Queue>) {
    let physical_device = PhysicalDevice::from_index(&instance, physical_device_index).unwrap();
    let indices = find_queue_families(&surface, &physical_device);

    let families = [indices.graphics_family, indices.present_family];
    use std::iter::FromIterator;
    let unique_queue_families: HashSet<&i32> = HashSet::from_iter(families.iter());

    let queue_priority = 1.0;
    let queue_families = unique_queue_families.iter().map(|i| {
        (
            physical_device.queue_families().nth(**i as usize).unwrap(),
            queue_priority,
        )
    });

    // NOTE: the tutorial recommends passing the validation layers as well
    // for legacy reasons (if ENABLE_VALIDATION_LAYERS is true). Vulkano handles that
    // for us internally.

    let (device, mut queues) = Device::new(
        physical_device,
        &Features::none(),
        &device_extensions(),
        queue_families,
    )
    .expect("failed to create logical device!");

    let graphics_queue = queues.next().unwrap();
    let present_queue = queues.next().unwrap_or_else(|| graphics_queue.clone());

    (device, graphics_queue, present_queue)
}

pub fn create_swap_chain(
    instance: &Arc<Instance>,
    surface: &Arc<Surface<Window>>,
    physical_device_index: usize,
    device: &Arc<Device>,
    graphics_queue: &Arc<Queue>,
    present_queue: &Arc<Queue>,
    dimensions: [u32; 2],
    old_swapchain: Option<Arc<Swapchain<Window>>>,
) -> (Arc<Swapchain<Window>>, Vec<Arc<SwapchainImage<Window>>>) {
    let physical_device = PhysicalDevice::from_index(&instance, physical_device_index).unwrap();
    let capabilities = surface
        .capabilities(physical_device)
        .expect("failed to get surface capabilities");

    let surface_format = choose_swap_surface_format(&capabilities.supported_formats);
    let present_mode = choose_swap_present_mode(capabilities.present_modes);
    let extent = choose_swap_extent(&capabilities, dimensions);

    let mut image_count = capabilities.min_image_count + 1;
    if capabilities.max_image_count.is_some() && image_count > capabilities.max_image_count.unwrap()
    {
        image_count = capabilities.max_image_count.unwrap();
    }

    let image_usage = ImageUsage {
        color_attachment: true,
        ..ImageUsage::none()
    };

    let indices = find_queue_families(&surface, &physical_device);

    let sharing: SharingMode = if indices.graphics_family != indices.present_family {
        vec![graphics_queue, present_queue].as_slice().into()
    } else {
        graphics_queue.into()
    };

    let (swap_chain, images) = Swapchain::new(
        device.clone(),
        surface.clone(),
        image_count,
        surface_format.0, // TODO: color space?
        extent,
        1, // layers
        image_usage,
        sharing,
        capabilities.current_transform,
        CompositeAlpha::Opaque,
        present_mode,
        true, // clipped
        old_swapchain.as_ref(),
    )
    .expect("failed to create swap chain!");

    (swap_chain, images)
}

pub fn create_sync_objects(device: &Arc<Device>) -> Box<GpuFuture> {
    Box::new(sync::now(device.clone())) as Box<GpuFuture>
}

fn choose_swap_surface_format(available_formats: &[(Format, ColorSpace)]) -> (Format, ColorSpace) {
    // NOTE: the 'preferred format' mentioned in the tutorial doesn't seem to be
    // queryable in Vulkano (no VK_FORMAT_UNDEFINED enum)
    *available_formats
        .iter()
        .find(|(format, color_space)| {
            *format == Format::B8G8R8A8Unorm && *color_space == ColorSpace::SrgbNonLinear
        })
        .unwrap_or_else(|| &available_formats[0])
}

fn choose_swap_present_mode(available_present_modes: SupportedPresentModes) -> PresentMode {
    if available_present_modes.mailbox {
        PresentMode::Mailbox
    } else if available_present_modes.immediate {
        PresentMode::Immediate
    } else {
        PresentMode::Fifo
    }
}
fn choose_swap_extent(capabilities: &Capabilities, dimensions: [u32; 2]) -> [u32; 2] {
    if let Some(current_extent) = capabilities.current_extent {
        return current_extent;
    } else {
        let mut actual_extent = dimensions;
        actual_extent[0] = capabilities.min_image_extent[0]
            .max(capabilities.max_image_extent[0].min(actual_extent[0]));
        actual_extent[1] = capabilities.min_image_extent[1]
            .max(capabilities.max_image_extent[1].min(actual_extent[1]));
        actual_extent
    }
}

pub fn create_graphics_pipeline(
    device: &Arc<Device>,
    swap_chain_extent: [u32; 2],
    render_pass: &Arc<RenderPassAbstract + Send + Sync>,
) -> Arc<GraphicsPipelineAbstract + Send + Sync> {
    mod vertex_shader {
        vulkano_shaders::shader! {
            ty: "vertex",
            path: "src/shaders/base_vert.glsl"
        }
    }
    mod fragment_shader {
        vulkano_shaders::shader! {
            ty: "fragment",
            path: "src/shaders/base_frag.glsl"
        }
    }
    let vert_shader_module = vertex_shader::Shader::load(device.clone())
        .expect("Failed to create certex shader module!");
    let frag_shader_module = fragment_shader::Shader::load(device.clone())
        .expect("Failed to create fragment shader module!");
    let dimensions = [swap_chain_extent[0] as f32, swap_chain_extent[1] as f32];
    let viewport = Viewport {
        origin: [0.0, 0.0],
        dimensions,
        depth_range: 0.0..1.0,
    };
    Arc::new(
        GraphicsPipeline::start()
            .vertex_input_single_buffer::<Vertex>()
            .vertex_shader(vert_shader_module.main_entry_point(), ())
            .triangle_list()
            .primitive_restart(false)
            .viewports(vec![viewport]) // NOTE: also sets scissor to cover whole viewport
            .fragment_shader(frag_shader_module.main_entry_point(), ())
            .depth_clamp(false)
            // NOTE: there's an outcommend .rasterized_discard() in Vulkano
            .polygon_mode_fill()
            .line_width(1.0)
            .cull_mode_back()
            .front_face_counter_clockwise()
            // NOTE: no depth_bias here, but on pipeline::raster::Rasterization
            .blend_pass_through()
            .depth_stencil(DepthStencil::simple_depth_test())
            .render_pass(Subpass::from(render_pass.clone(), 0).unwrap())
            .build(device.clone())
            .unwrap(),
    )
}

pub fn create_depth_image(
    device: &Arc<Device>,
    dimensions: [u32; 2],
    format: Format,
    sample_count: u32,
) -> Arc<AttachmentImage<Format>> {
    AttachmentImage::multisampled_with_usage(
        device.clone(),
        dimensions,
        sample_count,
        format,
        ImageUsage {
            depth_stencil_attachment: true,
            ..ImageUsage::none()
        },
    )
    .unwrap()
}

pub fn create_render_pass(
    device: &Arc<Device>,
    color_format: Format,
    depth_format: Format,
    sample_count: u32,
) -> Arc<RenderPassAbstract + Send + Sync> {
    Arc::new(
        single_pass_renderpass!(device.clone(),
            attachments: {
                multisample_color: {
                    load: Clear,
                    store: Store,
                    format: color_format,
                    samples: sample_count,
                },
                multisample_depth: {
                    load: Clear,
                    store: DontCare,
                    format: depth_format,
                    samples: sample_count,
                    initial_layout: ImageLayout::Undefined,
                    final_layout: ImageLayout::DepthStencilAttachmentOptimal,
                },
                resolve_color: {
                    load: DontCare,
                    store: Store,
                    format: color_format,
                    samples: 1,
                }
            },
            pass: {
                color: [multisample_color],
                depth_stencil: {multisample_depth}
                resolve: [resolve_color]
            }
        )
        .unwrap(),
    )
}

pub fn create_framebuffers(
    device: &Arc<Device>,
    swap_chain_images: &Vec<Arc<SwapchainImage<Window>>>,
    render_pass: &Arc<RenderPassAbstract + Send + Sync>,
    depth_image: &Arc<AttachmentImage<Format>>,
    sample_count: u32,
) -> Vec<Arc<FramebufferAbstract + Send + Sync>> {
    swap_chain_images
        .iter()
        .map(|image| {
            let dim = image.dimensions().width_height();
            let multisampled_image = AttachmentImage::transient_multisampled(
                device.clone(),
                dim,
                sample_count,
                image.format(),
            )
            .unwrap();
            let fba: Arc<FramebufferAbstract + Send + Sync> = Arc::new(
                Framebuffer::start(render_pass.clone())
                    .add(multisampled_image.clone())
                    .unwrap()
                    .add(depth_image.clone())
                    .unwrap()
                    .add(image.clone())
                    .unwrap()
                    .build()
                    .unwrap(),
            );
            fba
        })
        .collect::<Vec<_>>()
}

pub fn create_descriptor_sets(
    graphics_pipeline: &Arc<GraphicsPipelineAbstract + Send + Sync>,
    uniform_buffers: &[Arc<CpuAccessibleBuffer<UniformBufferObject>>],
    texture_image: &Arc<ImmutableImage<Format>>,
    image_sampler: &Arc<Sampler>,
) -> DescriptorSet {
    let pool = create_descriptor_pool(graphics_pipeline);
    uniform_buffers
        .iter()
        .map(|uniform_buffer| {
            Arc::new(
                pool.lock()
                    .unwrap()
                    .next()
                    .add_buffer(uniform_buffer.clone())
                    .unwrap()
                    .add_sampled_image(texture_image.clone(), image_sampler.clone())
                    .unwrap()
                    .build()
                    .unwrap(),
            )
        })
        .collect()
}

pub fn create_descriptor_pool(
    graphics_pipeline: &Arc<GraphicsPipelineAbstract + Send + Sync>,
) -> Arc<Mutex<FixedSizeDescriptorSetsPool<Arc<GraphicsPipelineAbstract + Send + Sync>>>> {
    Arc::new(Mutex::new(FixedSizeDescriptorSetsPool::new(
        graphics_pipeline.clone(),
        0,
    )))
}

pub fn create_command_buffers(
    device: &Arc<Device>,
    graphics_queue: &Arc<Queue>,
    vertex_buffer: &Arc<BufferAccess + Send + Sync>,
    index_buffer: &Arc<TypedBufferAccess<Content = [u32]> + Send + Sync>,
    uniform_buffers: &Vec<Arc<CpuAccessibleBuffer<UniformBufferObject>>>,
    graphics_pipeline: &Arc<GraphicsPipelineAbstract + Send + Sync>,
    start_time: Instant,
    dimensions: [u32; 2],
    swap_chain_framebuffers: &Vec<Arc<FramebufferAbstract + Send + Sync>>,
    descriptor_sets: &DescriptorSet,
    controller: &DynamicViewport,
) -> Vec<Arc<AutoCommandBuffer>> {
    let queue_family = graphics_queue.family();
    let dimensions = [dimensions[0] as f32, dimensions[1] as f32];
    swap_chain_framebuffers
        .iter()
        .enumerate()
        .map(|(i, framebuffer)| {
            Arc::new(
                AutoCommandBufferBuilder::primary_simultaneous_use(device.clone(), queue_family)
                    .unwrap()
                    .update_buffer(
                        uniform_buffers[i].clone(),
                        update_uniform_buffer(start_time, dimensions, controller),
                    )
                    .unwrap()
                    .begin_render_pass(
                        framebuffer.clone(),
                        false,
                        vec![
                            [0.0, 0.0, 0.0, 1.0].into(),
                            ClearValue::Depth(1.0),
                            ClearValue::None,
                        ],
                    )
                    .unwrap()
                    .draw_indexed(
                        graphics_pipeline.clone(),
                        &DynamicState::none(),
                        vec![vertex_buffer.clone()],
                        index_buffer.clone(),
                        descriptor_sets[i].clone(),
                        (),
                    )
                    .unwrap()
                    .end_render_pass()
                    .unwrap()
                    .build()
                    .unwrap(),
            )
        })
        .collect()
}

pub fn create_vertex_buffer(
    graphics_queue: &Arc<Queue>,
    vertices: Vec<Vertex>,
) -> Arc<BufferAccess + Send + Sync> {
    let (buffer, future) = ImmutableBuffer::from_iter(
        vertices.into_iter(),
        BufferUsage::vertex_buffer(),
        graphics_queue.clone(),
    )
    .unwrap();
    future.flush().unwrap();
    buffer
}

pub fn create_index_buffer(
    graphics_queue: &Arc<Queue>,
    indices: Vec<u32>,
) -> Arc<TypedBufferAccess<Content = [u32]> + Send + Sync> {
    let (buffer, future) = ImmutableBuffer::from_iter(
        indices.into_iter(),
        BufferUsage::index_buffer(),
        graphics_queue.clone(),
    )
    .unwrap();
    future.flush().unwrap();
    buffer
}

pub fn create_uniform_buffers(
    device: &Arc<Device>,
    num_buffers: usize,
    start_time: Instant,
    dimensions: [u32; 2],
    controller: &DynamicViewport,
) -> Vec<Arc<CpuAccessibleBuffer<UniformBufferObject>>> {
    let mut buffers = Vec::new();
    let dimensions = [dimensions[0] as f32, dimensions[1] as f32];
    let uniform_buffer = update_uniform_buffer(start_time, dimensions, controller);

    for _ in 0..num_buffers {
        let buffer = CpuAccessibleBuffer::from_data(
            device.clone(),
            BufferUsage::uniform_buffer_transfer_destination(),
            uniform_buffer,
        )
        .unwrap();
        buffers.push(buffer);
    }
    buffers
}

pub fn find_sample_count() -> u32 {
    4
}

pub fn find_depth_format() -> Format {
    Format::D16Unorm
}
