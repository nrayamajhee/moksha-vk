use std::sync::Arc;
use vulkano::{instance::Instance, swapchain::Surface};
use vulkano_win::VkSurfaceBuild;
use winit::{dpi::LogicalSize, EventsLoop, Window as WinitWindow, WindowBuilder};

#[derive(Copy, Clone)]
pub struct WindowConfig {
    pub title: &'static str,
    pub width: u32,
    pub height: u32,
    pub fullscreen: bool,
}

pub struct Window {
    pub events_loop: EventsLoop,
    pub surface: Arc<Surface<WinitWindow>>,
}
impl Window {
    pub fn new(instance: &Arc<Instance>, cfg: &WindowConfig) -> Self {
        let events_loop = EventsLoop::new();
        let surface = WindowBuilder::new()
            .with_title(cfg.title)
            .with_dimensions(LogicalSize::new(
                f64::from(cfg.width),
                f64::from(cfg.height),
            ))
            .build_vk_surface(&events_loop, instance.clone())
            .expect("failed to create window surface!");
        let window = surface.window();
        #[cfg(feature = "icon_loading")]
        {
            use winit::Icon;
            let path = concat!(env!("CARGO_MANIFEST_DIR"), "/src/assets/img/icon.png");
            let icon = Icon::from_path(path).expect("Failed to load icon!");
            window.set_window_icon(Some(icon));
        }
        if cfg.fullscreen {
            window.set_fullscreen(Some(window.get_current_monitor()));
        }
        Window {
            events_loop,
            surface,
        }
    }
}
