use winit::{
    ElementState,
    MouseButton,
};

#[derive(Debug, Clone, Copy)]
pub struct MouseEvent {
    pub button: Option<MouseButton>,
    pub state: ElementState,
    pub cursor_delta: (f64, f64),
    pub scroll_delta: f32,
}
#[derive(Debug, Clone, Copy)]
pub struct ViewportEvent {
    pub mouse_event: MouseEvent,
    pub aspect_ratio: Option<f64>,
    pub reset_view: bool,
    pub increment_sensitivity: Option<(bool, Option<f32>)>,
}
impl MouseEvent {
    pub fn new(button: Option<winit::MouseButton>) -> Self {
        Self {
            button,
            state: ElementState::Released,
            cursor_delta: (0., 0.),
            scroll_delta: 0.,
        }
    }
    pub fn reset_delta(&mut self) {
        self.cursor_delta = (0.,0.);
        self.scroll_delta = 0.;
    }
}
impl ViewportEvent {
    pub fn new(button: Option<winit::MouseButton>) -> Self {
        let mouse_event = MouseEvent::new(button);
        Self {
            mouse_event,
            aspect_ratio: None,
            reset_view: false,
            increment_sensitivity: None,
        }
    }
    pub fn reset(&mut self) {
        self.mouse_event.reset_delta();
        self.aspect_ratio = None;
        self.reset_view = false;
        self.increment_sensitivity = None;
    }
}