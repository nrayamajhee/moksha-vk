use crate::{
    viewport::{DynamicViewport, PerspectiveProjection},
    IcoSphere, UniformBufferObject, Vertex,
};
use cgmath::{Deg, Matrix4, Point3, Rad, Vector3};
use noise::{Fbm, NoiseFn};
use std::time::Instant;
use tobj::load_obj;

pub fn flatten_mesh(meshes: &Vec<IcoSphere>) -> (Vec<Vertex>, Vec<u32>) {
    let mut noise = Fbm::new();
    noise.octaves = 6;
    noise.frequency = 1.;
    noise.lacunarity = 3.;
    noise.persistence = 0.5;

    let mut vertices: Vec<Vertex> = Vec::new();
    let mut indices: Vec<u32> = Vec::new();
    for mesh in meshes {
        for v in &mesh.vertices {
            let mut d = noise.get([v[0] as f64, v[1] as f64, v[2] as f64]) as f32;
            d = 1.0 + 0.1 * d;
            let vert = [v[0] * d, v[1] * d, v[2] * d];
            vertices.push(Vertex::new(vert, [0.0, 1.0, 0.0], [1.0, 0.0]));
            d = 1.01;
            let vert = [v[0] * d, v[1] * d, v[2] * d];
            vertices.push(Vertex::new(vert, [0.0, 0.0, 1.0], [1.0, 0.0]));
        }
        let offset = *(&mesh.faces.len());
        for i in &mesh.faces {
            let a = i[0] * 2;
            let b = i[1] * 2;
            let c = i[2] * 2;

            indices.push(a as u32);
            indices.push(b as u32);
            indices.push(c as u32);

            indices.push((a + 1) as u32);
            indices.push((b + 1) as u32);
            indices.push((c + 1) as u32);
        }
    }
    (vertices, indices)
    // (vec![
    //     Vertex::new([-0.5, -0.5, 0.0], [1.0, 0.0, 0.0], [1.0, 0.0]),
    //     Vertex::new([0.5, -0.5, 0.0], [0.0, 1.0, 0.0], [0.0, 0.0]),
    //     Vertex::new([0.5, 0.5, 0.0], [0.0, 0.0, 1.0], [0.0, 1.0]),
    //     Vertex::new([-0.5, 0.5, 0.0], [1.0, 1.0, 1.0], [1.0, 1.0]),

    //     Vertex::new([-0.5, -0.5, -0.5], [1.0, 0.0, 0.0], [1.0, 0.0]),
    //     Vertex::new([0.5, -0.5, -0.5], [0.0, 1.0, 0.0], [0.0, 0.0]),
    //     Vertex::new([0.5, 0.5, -0.5], [0.0, 0.0, 1.0], [0.0, 1.0]),
    //     Vertex::new([-0.5, 0.5, -0.5], [1.0, 1.0, 1.0], [1.0, 1.0])
    // ],
    // vec![
    //     0, 1, 2, 2, 3, 0,
    //     4, 5, 6, 6, 7, 4,
    // ])
}

pub fn load_model(path: &str) -> (Vec<Vertex>, Vec<u32>) {
    let mut vertices = Vec::new();
    let mut indices = Vec::new();

    let (models, _materials) = load_obj(path.as_ref()).unwrap();

    for model in models.iter() {
        let mesh = &model.mesh;

        for index in &mesh.indices {
            let ind_usize = *index as usize;
            let pos = [
                mesh.positions[ind_usize * 3],
                mesh.positions[ind_usize * 3 + 1],
                mesh.positions[ind_usize * 3 + 2],
            ];

            let color = [1.0, 1.0, 1.0];

            let tex_coord = [
                mesh.texcoords[ind_usize * 2],
                1.0 - mesh.texcoords[ind_usize * 2 + 1],
            ];

            let vertex = Vertex::new(pos, color, tex_coord);
            vertices.push(vertex);
            let index = indices.len() as u32;
            indices.push(index);
        }
    }
    (vertices, indices)
}

pub fn update_uniform_buffer(
    start_time: Instant,
    dimensions: [f32; 2],
    controller: &DynamicViewport,
) -> UniformBufferObject {
    let duration = start_time.elapsed();
    let elapsed = (duration.as_secs() * 1000) + u64::from(duration.subsec_millis());

    let model = Matrix4::from_angle_y(Rad::from(Deg(0.)));
    // let model = Matrix4::from_angle_y(Rad::from(Deg(elapsed as f32 * 0.018)));

    let mut proj = controller.projection_matrix;
    proj.y *= -1.0;

    UniformBufferObject {
        model,
        view: controller.view_matrix,
        proj,
    }
}
