use cgmath::Matrix4;
use vulkano::impl_vertex;

#[derive(Copy, Clone, Debug)]
pub struct Vertex {
    pos: [f32; 3],
    color: [f32; 3],
    tex: [f32; 2],
}

impl Vertex {
    pub fn new(pos: [f32; 3], color: [f32; 3], tex: [f32; 2]) -> Self {
        Self { pos, color, tex }
    }
}

impl_vertex!(Vertex, pos, color, tex);

pub struct QueueFamilyIndices {
    pub graphics_family: i32,
    pub present_family: i32,
}

impl QueueFamilyIndices {
    pub fn new() -> Self {
        Self {
            graphics_family: -1,
            present_family: -1,
        }
    }

    pub fn is_complete(&self) -> bool {
        self.graphics_family >= 0 && self.present_family >= 0
    }
}

#[derive(Copy, Clone)]
pub struct UniformBufferObject {
    pub model: Matrix4<f32>,
    pub view: Matrix4<f32>,
    pub proj: Matrix4<f32>,
}
