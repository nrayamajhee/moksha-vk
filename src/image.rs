use image::GenericImageView;
use std::sync::Arc;
use vulkano::{
    buffer::{BufferUsage, CpuAccessibleBuffer},
    command_buffer::{AutoCommandBufferBuilder, CommandBuffer},
    device::{Device, Queue},
    format::Format,
    image::{
        Dimensions, ImageAccess, ImageDimensions, ImageLayout, ImageUsage, ImmutableImage,
        MipmapsCount,
    },
    sampler::{Filter, MipmapMode, Sampler, SamplerAddressMode},
    sync::GpuFuture,
};

fn get_mip_dim(mip_idx: u32, img_dimension: ImageDimensions) -> Result<[i32; 3], String> {
    if let Some(dim) = img_dimension.mipmap_dimensions(mip_idx) {
        if let ImageDimensions::Dim2d { width, height, .. } = dim {
            Ok([width as i32, height as i32, 1])
        } else {
            Err("MipMapping: Did not get 2D image for blitting".to_string())
        }
    } else {
        Err(format!("MipMapping: image has no mip map at level {}", mip_idx).to_string())
    }
}

pub fn create_texture(queue: &Arc<Queue>) -> Arc<ImmutableImage<Format>> {
    let path = concat!(env!("CARGO_MANIFEST_DIR"), "/src/assets/img/chalet.jpg");
    let image = image::open(path).unwrap();
    let dimensions = Dimensions::Dim2d {
        width: image.width(),
        height: image.height(),
    };

    let image_rgba = image.to_rgba();

    let image_usage = ImageUsage {
        transfer_destination: true,
        transfer_source: true,
        sampled: true,
        ..ImageUsage::none()
    };

    let (image, image_init) = ImmutableImage::uninitialized(
        queue.device().clone(),
        dimensions,
        Format::R8G8B8A8Unorm,
        MipmapsCount::Log2,
        image_usage,
        ImageLayout::TransferDstOptimal,
        queue.device().active_queue_families(),
    )
    .unwrap();

    let source = CpuAccessibleBuffer::from_iter(
        queue.device().clone(),
        BufferUsage {
            transfer_source: true,
            ..BufferUsage::none()
        },
        image_rgba.into_raw().iter().cloned(),
    )
    .unwrap();

    let mut cb = AutoCommandBufferBuilder::new(queue.device().clone(), queue.family())
        .expect("Failed to start command buffer for image creation!");

    cb = cb
        .copy_buffer_to_image_dimensions(
            source,
            image_init,
            [0, 0, 0],
            image.dimensions().width_height_depth(),
            0,
            image.dimensions().array_layers(),
            0,
        )
        .unwrap();

    let img_dimension = ImageAccess::dimensions(&image);

    for mip_idx in 1..image.mipmap_levels() {
        let source_dim = get_mip_dim(mip_idx - 1, img_dimension).unwrap();
        let dest_dim = get_mip_dim(mip_idx, img_dimension).unwrap();

        cb = cb
            .blit_image(
                image.clone(),
                [0; 3],
                source_dim,
                0,
                mip_idx - 1,
                image.clone(),
                [0; 3],
                dest_dim,
                0,
                mip_idx,
                1,
                Filter::Linear,
            )
            .unwrap();
    }

    // todo -> figure out if i need to divide mipWidth and mipHeight by 2
    let final_cb = cb
        .build()
        .expect("failed to build MipMapping command buffer");
    let future = final_cb.execute(queue.clone()).unwrap();
    future.flush().unwrap();
    image
}

pub fn create_image_sampler(device: &Arc<Device>) -> Arc<Sampler> {
    // Sampler::simple_repeat_linear(device.clone())

    let mip_lod = 0.0;
    Sampler::new(
        device.clone(),
        Filter::Linear,
        Filter::Linear,
        MipmapMode::Linear,
        SamplerAddressMode::Repeat,
        SamplerAddressMode::Repeat,
        SamplerAddressMode::Repeat,
        0.0,
        1.0,
        mip_lod,
        1_000.0,
    )
    .unwrap()
}
