use crate::{
    geometry::{flatten_mesh, load_model},
    image::{create_image_sampler, create_texture},
    viewport::{DynamicViewport, PerspectiveProjection},
    events::ViewportEvent,
    vk_factory::{
        create_command_buffers, create_depth_image, create_descriptor_sets, create_framebuffers,
        create_graphics_pipeline, create_index_buffer, create_instance, create_logical_device,
        create_render_pass, create_swap_chain, create_sync_objects, create_uniform_buffers,
        create_vertex_buffer, find_depth_format, find_sample_count, pick_physical_device,
        setup_debug_callback, DescriptorSet,
    },
    IcoSphere, UniformBufferObject, Window, WindowConfig,
};
use std::sync::Arc;
use std::time::Instant;
use vulkano::{
    buffer::{BufferAccess, CpuAccessibleBuffer, TypedBufferAccess},
    command_buffer::AutoCommandBuffer,
    device::{Device, Queue},
    format::Format,
    framebuffer::{FramebufferAbstract, RenderPassAbstract},
    image::swapchain::SwapchainImage,
    instance::{debug::DebugCallback, Instance},
    pipeline::GraphicsPipelineAbstract,
    swapchain::{acquire_next_image, AcquireError, Swapchain},
    sync::GpuFuture,
};
use winit::{
    dpi::LogicalPosition, dpi::LogicalSize, DeviceEvent, Event, KeyboardInput, MouseButton,
    VirtualKeyCode, Window as WinitWindow, WindowEvent,
};

pub struct Moksha {
    viewport: DynamicViewport,

    instance: Arc<Instance>,
    debug_callback: Option<DebugCallback>,

    window: Window,

    physical_device_index: usize,
    device: Arc<Device>,

    graphics_queue: Arc<Queue>,
    present_queue: Arc<Queue>,

    swap_chain: Arc<Swapchain<WinitWindow>>,
    swap_chain_images: Vec<Arc<SwapchainImage<WinitWindow>>>,

    render_pass: Arc<RenderPassAbstract + Send + Sync>,
    graphics_pipeline: Arc<GraphicsPipelineAbstract + Send + Sync>,

    swap_chain_framebuffers: Vec<Arc<FramebufferAbstract + Send + Sync>>,

    mesh_pool: Vec<IcoSphere>,

    vertex_buffer: Arc<BufferAccess + Send + Sync>,
    index_buffer: Arc<TypedBufferAccess<Content = [u32]> + Send + Sync>,

    descriptor_sets: DescriptorSet,
    uniform_buffers: Vec<Arc<CpuAccessibleBuffer<UniformBufferObject>>>,

    command_buffers: Vec<Arc<AutoCommandBuffer>>,
    depth_format: Format,

    sample_count: u32,

    previous_frame_end: Option<Box<GpuFuture>>,
    recreate_swap_chain: bool,

    start_time: Instant,
}

impl Moksha {
    pub fn new(cfg: &WindowConfig) -> Self {
        let instance = create_instance();
        let window = Window::new(&instance, cfg);
        let debug_callback = setup_debug_callback(&instance);

        let physical_device_index = pick_physical_device(&instance, &window.surface);
        let (device, graphics_queue, present_queue) =
            create_logical_device(&instance, &window.surface, physical_device_index);
        let (swap_chain, swap_chain_images) = create_swap_chain(
            &instance,
            &window.surface,
            physical_device_index,
            &device,
            &graphics_queue,
            &present_queue,
            [cfg.width, cfg.height],
            None,
        );

        let depth_format = find_depth_format();
        let sample_count = find_sample_count();
        let depth_image =
            create_depth_image(&device, swap_chain.dimensions(), depth_format, sample_count);
        let render_pass =
            create_render_pass(&device, swap_chain.format(), depth_format, sample_count);

        let graphics_pipeline =
            create_graphics_pipeline(&device, swap_chain.dimensions(), &render_pass);
        let swap_chain_framebuffers = create_framebuffers(
            &device,
            &swap_chain_images,
            &render_pass,
            &depth_image,
            sample_count,
        );

        let mesh_pool = vec![IcoSphere::subdivide(4)];

        let (vertices, indices) = flatten_mesh(&mesh_pool);
        // let path = concat!(env!("CARGO_MANIFEST_DIR"),"/src/assets/obj/chalet.obj");
        // let (vertices, indices) = load_model(path);

        let vertex_buffer = create_vertex_buffer(&graphics_queue, vertices);
        let index_buffer = create_index_buffer(&graphics_queue, indices);

        let proj = PerspectiveProjection::new(45., 0.1, 10.);
        let viewport = DynamicViewport::new(proj, swap_chain.dimensions());

        let start_time = Instant::now();
        let uniform_buffers = create_uniform_buffers(
            &device,
            swap_chain_images.len(),
            start_time,
            swap_chain.dimensions(),
            &viewport,
        );

        let texture_image = create_texture(&graphics_queue);
        let image_sampler = create_image_sampler(&device);

        let descriptor_sets = create_descriptor_sets(
            &graphics_pipeline,
            &uniform_buffers,
            &texture_image,
            &image_sampler,
        );

        let command_buffers = create_command_buffers(
            &device,
            &graphics_queue,
            &vertex_buffer,
            &index_buffer,
            &uniform_buffers,
            &graphics_pipeline,
            start_time,
            swap_chain.dimensions(),
            &swap_chain_framebuffers,
            &descriptor_sets,
            &viewport,
        );
        let previous_frame_end = Some(create_sync_objects(&device));
        println!("First frame complete!");
        Self {
            viewport,

            instance,
            debug_callback,

            window,

            physical_device_index,
            device,

            graphics_queue,
            present_queue,

            swap_chain,
            swap_chain_images,

            render_pass,
            graphics_pipeline,

            swap_chain_framebuffers,

            mesh_pool,

            vertex_buffer,
            index_buffer,

            uniform_buffers,
            descriptor_sets,

            command_buffers,
            depth_format,
            sample_count,

            previous_frame_end,
            recreate_swap_chain: false,

            start_time,
        }
    }
    pub fn update(&mut self) {
        let mut done = false;
        let mut viewport_event = ViewportEvent::new(self.viewport.button);
        loop {
            viewport_event.reset();
            self.window.events_loop.poll_events(|ev| match ev {
                Event::WindowEvent {
                    event: WindowEvent::CloseRequested,
                    ..
                } => done = true,
                Event::WindowEvent {
                    event: WindowEvent::Resized(LogicalSize { width, height }),
                    ..
                } => {
                    viewport_event.aspect_ratio = Some(width / height);
                }
                Event::WindowEvent {
                    event:
                        WindowEvent::MouseWheel {
                            delta: winit::MouseScrollDelta::LineDelta(_, s),
                            ..
                        },
                    ..
                } => viewport_event.mouse_event.scroll_delta = s,
                Event::DeviceEvent {
                    event: DeviceEvent::MouseMotion { delta: (x, y), .. },
                    ..
                } => viewport_event.mouse_event.cursor_delta = (x, y),
                Event::WindowEvent {
                    event: WindowEvent::MouseInput { button, state, .. },
                    ..
                } => {
                    viewport_event.mouse_event.button = Some(button);
                    viewport_event.mouse_event.state = state;
                }
                Event::DeviceEvent {
                    event:
                        DeviceEvent::Key(KeyboardInput {
                            virtual_keycode: Some(key),
                            ..
                        }),
                    ..
                } => {
                    if key == VirtualKeyCode::R {
                        viewport_event.reset_view = true;
                    }
                    if key == VirtualKeyCode::Escape {
                        done = true;
                    }
                    if key == VirtualKeyCode::Equals {
                        viewport_event.increment_sensitivity = Some((false, None));
                    }
                    if key == VirtualKeyCode::Subtract || key == VirtualKeyCode::Minus {
                        viewport_event.increment_sensitivity = Some((false, Some(-1.)));
                    }
                    if key == VirtualKeyCode::Key0 {
                        viewport_event.increment_sensitivity = Some((true, None));
                    }
                }
                _ => (),
            });
            if done {
                return;
            };
            self.viewport.update(&viewport_event);
            self.command_buffers = create_command_buffers(
                &self.device,
                &self.graphics_queue,
                &self.vertex_buffer,
                &self.index_buffer,
                &self.uniform_buffers,
                &self.graphics_pipeline,
                self.start_time,
                self.swap_chain.dimensions(),
                &self.swap_chain_framebuffers,
                &self.descriptor_sets,
                &self.viewport,
            );
            self.draw_frame();
        }
    }
    fn draw_frame(&mut self) {
        self.previous_frame_end.as_mut().unwrap().cleanup_finished();
        if self.recreate_swap_chain {
            self.recreate_swap_chain();
            self.recreate_swap_chain = false;
        }
        let (image_index, acquire_future) = match acquire_next_image(self.swap_chain.clone(), None)
        {
            Ok(r) => r,
            Err(AcquireError::OutOfDate) => {
                self.recreate_swap_chain = true;
                return;
            }
            Err(err) => panic!("{:?}", err),
        };
        let command_buffer = self.command_buffers[image_index].clone();
        let future = self
            .previous_frame_end
            .take()
            .unwrap()
            .join(acquire_future)
            .then_execute(self.graphics_queue.clone(), command_buffer)
            .unwrap()
            .then_swapchain_present(
                self.present_queue.clone(),
                self.swap_chain.clone(),
                image_index,
            )
            .then_signal_fence_and_flush();

        match future {
            Ok(future) => {
                self.previous_frame_end = Some(Box::new(future) as Box<_>);
            }
            Err(vulkano::sync::FlushError::OutOfDate) => {
                self.recreate_swap_chain = true;
                self.previous_frame_end =
                    Some(Box::new(vulkano::sync::now(self.device.clone())) as Box<_>);
            }
            Err(e) => {
                println!("{:?}", e);
                self.previous_frame_end =
                    Some(Box::new(vulkano::sync::now(self.device.clone())) as Box<_>);
            }
        }
    }
    fn recreate_swap_chain(&mut self) {
        let (swap_chain, images) = create_swap_chain(
            &self.instance,
            &self.window.surface,
            self.physical_device_index,
            &self.device,
            &self.graphics_queue,
            &self.present_queue,
            self.swap_chain.dimensions(),
            Some(self.swap_chain.clone()),
        );
        let depth_image = create_depth_image(
            &self.device,
            swap_chain.dimensions(),
            self.depth_format,
            self.sample_count,
        );
        self.swap_chain = swap_chain;
        self.swap_chain_images = images;
        self.render_pass = create_render_pass(
            &self.device,
            self.swap_chain.format(),
            self.depth_format,
            self.sample_count,
        );
        self.graphics_pipeline = create_graphics_pipeline(
            &self.device,
            self.swap_chain.dimensions(),
            &self.render_pass,
        );
        self.swap_chain_framebuffers = create_framebuffers(
            &self.device,
            &self.swap_chain_images,
            &self.render_pass,
            &depth_image,
            self.sample_count,
        );
        self.command_buffers = create_command_buffers(
            &self.device,
            &self.graphics_queue,
            &self.vertex_buffer,
            &self.index_buffer,
            &self.uniform_buffers,
            &self.graphics_pipeline,
            self.start_time,
            self.swap_chain.dimensions(),
            &self.swap_chain_framebuffers,
            &self.descriptor_sets,
            &self.viewport,
        );
    }
}
