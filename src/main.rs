use moksha::{Moksha, WindowConfig};

fn main() {
    let win_config = WindowConfig {
        title: "Moksha",
        width: 640,
        height: 480,
        fullscreen: false,
    };
    let mut app = Moksha::new(&win_config);
    app.update();
}
