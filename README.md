Here what follows is  bad english and bad computer codes. You may copy, modify, redistribute modified or verbatim copies according to one or both of the licenses mentioned below.

# Moksha

This is an experimental video game written in rust as a learning exercise. I intended to use as much help from libraries that the rust community offers to make a toy world with 3D graphics, pseudo physics, and sound effects.

# Task List

- Window
    - [x] Winit window with Vulkan surface
    - [ ] Use configuration file to creating window.
- Renderer
    - [x] Vertex, Index, and Uniform Buffers
    - [ ] Create a Mesh Pool to load vertices, indices, and textures
- Mesh
    - [x] load obj
    - [ ] Procedural Geometry
    - [x] Vertext Color
    - [x] Color Fill
    - [ ] Wireframe Line
    - [ ] Wireframe Line + Point + Depth fade
    - [ ] Procedulal Texure Nodes
    - [x] Albedo Texture
    - [ ] Normal Map/ Height Map
    - [ ] Dispalcement Map
    - [ ] Occlusion Map
    - [ ] Gltf Import
- Viewport
    - [x] Perspective Projection
    - [x] Third Person Perspective
    - [ ] Orthographic Projection
    - [ ] Fixed viewport
    - [ ] First Person Perspective
- Controller
    - [ ] Fly Navigation: accelerate, Deaccelerate, Roll, Pitch, and yaw movements
    - [ ] Walk Navigation: Acceleraete, Deaccelerate, Turn, Jump, Roll, Crouch, Crawl
    - [ ] Drive Navigation: Acceleraete, Deaccelerate, Turn


# Task List from Moksha-three.

I have dragged myself lower by writing against Vulkano instead of three. But hopefully these high level features will be implemented soon. The three-rs based code is available at <https://gitlab.com/nrayamajhee/moksha>. However, it is now abandoned.

- World
    - [ ] Create window, load milkyway skybox, and add plane model.
    - [ ] Use config file for loading skybox.
    - [ ] Create a debug mode that renders logs to the game window
    - [ ] Create a fps and debug text screen
    - [ ] Crate a 3D UI to display window config
- Plane
    - [ ] Custom third person perspective camera 
    - [ ] Basic movement
    - [ ] Accelerate, Deaccelerate, Roll, Pitch, and yaw
    - [ ] Gravity
- Planet
    - [ ] Displace the icosphere vertices with noise function
    - [ ] Multithread noise function and displacment
    - [ ] Create noise function once, and use it many times to update the planet
        geometry
    - [ ] Implement level of detail for icosphere vertices.

## How to?

To run the game:

```bash
git clone https://gitlab.com/nrayamajhee/moksha-vk.git
cd moksha-vk
cargo run
```

To read offline docs:

```bash
cargo doc --open
```

## License and Credits

Licensed under either of the following terms at your choice:

  - Apache License, Version 2.0, (LICENSE-APACHE or <http://www.apache.org/licenses/LICENSE-2.0>)
  - MIT license (LICENSE-MIT or <http://opensource.org/licenses/MIT>)

Unless you explicitly state otherwise, any contribution intentionally submitted for inclusion in the work by you, as defined in the Apache-2.0 license, shall be dual licensed as above, without any additional terms or conditions.

Data contained in the repository (images, gltf, blend, etc.) are licensed under Creative Commons Attribution 4.0 International License (CC BY 4.0).
Refer to <https://creativecommons.org/licenses/by/4.0/> for details.

Please refer to the links at /data/credits for individual attributions for the blend file used to generate the skybox, and the deep star map from NASA.

The vulkan codes to setup and initially draw traiangles have been directly copied from <https://github.com/bwasty/vulkan-tutorial-rs> and <https://github.com/mcr431/vulkan-tutorial-rs>. These repositories are under the "The Unlicense" which is released into the public domain. Also, note that these tutorials are base on Alexander Overvoorde's C++ vulkan tutorials availabe at <https://vulkan-tutorial.com>.