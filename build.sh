#!/bin/sh

case $1 in
    "compile" | "c")
        cargo build
    ;;
    "run" | "r")
        cargo run --features="icon_loading"
    ;;
    "doc" | "d")
        cargo doc --open
    ;;
    "clean" | "cl")
        cargo clean 
    ;;
    "backtrace" | "b")
        RUST_BACKTRACE=1 cargo run --features="icon_loading"
    ;;
    *)
        echo "build [compile(c) | run(r) | doc(d) | backtrace(b) | clean(cl)]"
    ;;
esac
